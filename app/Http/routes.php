<?php

use Carbon\Carbon;
use Illuminate\Http\Request;

$app->get('/', function () use ($app) {
    return view('home');
});

$app->post('/', function (Request $request) use ($app) {
    $url = $request->input('website');

    if (strlen($url) <= 1) {
        return redirect()->to('/');
    }

    $parts = parse_url($url);

    $hit = $parts['path'];

    if (isset($parts['host'])) {
        $hit = $parts['host'];
    }

    return redirect()->to($hit);
});

$app->get('{url:.*}', function ($url) {
    if (!\Cache::has($url)) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $code = $status === 200 ? 'online' : 'offline';

        \Cache::put($url, $code, Carbon::now()->addSeconds(60));
    }

    $status = \Cache::get($url);

    return view('status', compact('status', 'url'));
});
