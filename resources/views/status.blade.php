@extends('app')

@section('contents')
<h3 style="text-align: center;">
  It looks like {{ $url }} is {{ $status }}!
</h3>
@endsection
